<?php get_header(); ?>

<div id="step-1">
    <div id="global"></div>
    <div id="title">
    </div>
    <div id="menucontainer">
        <div id="menu">
            <div id="start">Start</div>
            <div id="s-controlType">Controls: Keyboard</div>
            <div id="s-quality">Quality: High</div>
            <div id="s-hud">HUD: On</div>
            <div id="s-godmode" style="display: none">Godmode: Off</div>
            <div id="s-credits">Credits</div>
        </div>
    </div>
</div>
<div id="step-2" style="display: none">
    <div id="ctrl-help">Click/Touch to continue.</div>
</div>
<div id="step-3" style="display: none">
    <div id="progressbar"></div>
</div>
<div id="step-4" style="display: none">
    <div id="overlay"></div>
    <div id="main"></div>
</div>
<div id="step-5" style="display: none">
    <div id="time"></div>
    <div id="ctrl-help">Click/Touch to continue.</div>
</div>
<div id="credits" style="display: none">
    <h3>Code</h3>
    <p><b>Concept and Development</b><br>Thibaut Despoulain (BKcore)</p>
    <p><b>Contributors</b><br>townxelliot<br>mahesh.kk</p>
    <p><b>Technologies</b><br>WebGL<br>JavaScript<br>CoffeeScript<br>Three.js<br>LeapMotion</p>
    <h3>Graphics</h3>
    <p><b>HexMKI base model</b><br>Charnel</p>
    <p><b>Track texture</b><br>Nobiax</p>
    <h4>Click anywhere to continue.</h4>
</div>

<div id="leapinfo" style="display: none"></div>

<?php get_footer(); ?>
