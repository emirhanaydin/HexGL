<?php
define('TEMPLATE_DIRECTORY_URI', get_template_directory_uri());

function enqueue_styles()
{
    wp_enqueue_style('multi', TEMPLATE_DIRECTORY_URI . '/css/multi.css', array(), null);
    wp_enqueue_style('fonts', TEMPLATE_DIRECTORY_URI . '/css/fonts.css', array(), null);
}

function enqueue_scripts()
{
    wp_enqueue_script('leap-0.4.1.min', TEMPLATE_DIRECTORY_URI . '/libs/leap-0.4.1.min.js', array(), null);
    wp_enqueue_script('Three.dev', TEMPLATE_DIRECTORY_URI . '/libs/Three.dev.js', array(), null);
    wp_enqueue_script('ShaderExtras', TEMPLATE_DIRECTORY_URI . '/libs/ShaderExtras.js', array(), null);
    wp_enqueue_script('EffectComposer', TEMPLATE_DIRECTORY_URI . '/libs/postprocessing/EffectComposer.js', array(), null);
    wp_enqueue_script('RenderPass', TEMPLATE_DIRECTORY_URI . '/libs/postprocessing/RenderPass.js', array(), null);
    wp_enqueue_script('BloomPass', TEMPLATE_DIRECTORY_URI . '/libs/postprocessing/BloomPass.js', array(), null);
    wp_enqueue_script('ShaderPass', TEMPLATE_DIRECTORY_URI . '/libs/postprocessing/ShaderPass.js', array(), null);
    wp_enqueue_script('MaskPass', TEMPLATE_DIRECTORY_URI . '/libs/postprocessing/MaskPass.js', array(), null);
    wp_enqueue_script('Detector', TEMPLATE_DIRECTORY_URI . '/libs/Detector.js', array(), null);
    wp_enqueue_script('Stats', TEMPLATE_DIRECTORY_URI . '/libs/Stats.js', array(), null);
    wp_enqueue_script('DAT.GUI.min', TEMPLATE_DIRECTORY_URI . '/libs/DAT.GUI.min.js', array(), null);

    wp_enqueue_script('TouchController', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/controllers/TouchController.js', array(), null);
    wp_enqueue_script('OrientationController', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/controllers/OrientationController.js', array(), null);
    wp_enqueue_script('GamepadController', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/controllers/GamepadController.js', array(), null);

    wp_enqueue_script('Timer', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/Timer.js', array(), null);
    wp_enqueue_script('ImageData', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/ImageData.js', array(), null);
    wp_enqueue_script('Utils', TEMPLATE_DIRECTORY_URI . '/bkcore.coffee/Utils.js', array(), null);

    wp_enqueue_script('RenderManager', TEMPLATE_DIRECTORY_URI . '/bkcore/threejs/RenderManager.js', array(), null);
    wp_enqueue_script('Shaders', TEMPLATE_DIRECTORY_URI . '/bkcore/threejs/Shaders.js', array(), null);
    wp_enqueue_script('Particles', TEMPLATE_DIRECTORY_URI . '/bkcore/threejs/Particles.js', array(), null);
    wp_enqueue_script('Loader', TEMPLATE_DIRECTORY_URI . '/bkcore/threejs/Loader.js', array(), null);

    wp_enqueue_script('Audio', TEMPLATE_DIRECTORY_URI . '/bkcore/Audio.js', array(), null);

    wp_enqueue_script('HUD', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/HUD.js', array(), null);
    wp_enqueue_script('RaceData', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/RaceData.js', array(), null);
    wp_enqueue_script('ShipControls', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/ShipControls.js', array(), null);
    wp_enqueue_script('ShipEffects', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/ShipEffects.js', array(), null);
    wp_enqueue_script('CameraChase', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/CameraChase.js', array(), null);
    wp_enqueue_script('Gameplay', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/Gameplay.js', array(), null);

    wp_enqueue_script('Cityscape', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/tracks/Cityscape.js', array(), null);

    wp_enqueue_script('HexGL', TEMPLATE_DIRECTORY_URI . '/bkcore/hexgl/HexGL.js', array(), null);

    wp_enqueue_script('launch', TEMPLATE_DIRECTORY_URI . '/launch.js', array(), null, true);
}

add_action('wp_enqueue_scripts', 'enqueue_styles');
add_action('wp_enqueue_scripts', 'enqueue_scripts');
