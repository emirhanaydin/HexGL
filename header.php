<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <title>HexGL</title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description"
          content="HexGL is a futuristic racing game built by Thibaut Despoulain (BKcore) using HTML5, Javascript and WebGL. Come challenge your friends on this fast-paced 3D game!">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <script>
        TEMPLATE_DIRECTORY_URI = "<?php echo TEMPLATE_DIRECTORY_URI; ?>";
    </script>
    <meta property="og:title" content="HexGL, the HTML5 futuristic racing game."/>
    <meta property="og:type" content="game"/>
    <meta property="og:image" content="<?php echo TEMPLATE_DIRECTORY_URI . '/icon_256.png'; ?>"/>
    <meta property="og:site_name" content="HexGL"/>
    <link rel="icon" href="<?php echo TEMPLATE_DIRECTORY_URI . "/favicon.png"; ?>" type="image/png">
    <link rel="shortcut icon" href="<?php echo TEMPLATE_DIRECTORY_URI . "/favicon.png"; ?>" type="image/png">
    <style>
        body {
            padding: 0;
            margin: 0;
        }

        canvas {
            pointer-events: none;
            width: 100%;
        }

        #overlay {
            position: absolute;
            z-index: 9999;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
